{pkgs ? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/36fd87baa9083f34f7f5027900b62ee6d09b1f2f.tar.gz") {}}:
pkgs.mkShellNoCC {
  packages = with pkgs; [
    (python3.withPackages (ps: [
      ps.beautifulsoup4
      ps.platformdirs
      ps.py7zr
      ps.unrardll
      ps.requests
      ps.tenacity
      ps.tqdm
      ps.magic
      ps.rarfile
      ps.gitpython
      ps.cloudscraper
      ps.pip
      ps.setuptools
      ps.build
      ps.installer
      ps.wheel
    ]))
    unrar
  ];
  UNRAR_LIB_PATH = "${pkgs.unrar}/lib/libunrar.so";
}
