# Flatpak

flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo
flatpak install com.valvesoftware.Steam
flatpak override --user --filesystem=/files com.valvesoftware.Steam
mkdir -p ~/.var/app/com.valvesoftware.Steam/data/Steam/compatibilitytools.d/

# Nix

sh <(curl -L https://nixos.org/nix/install) --daemon
nix-channel --add https://nixos.org/channels/nixpkgs-unstable
nix-channel --update

# nixGL

nix-channel --add https://github.com/guibou/nixGL/archive/main.tar.gz nixgl
nix-channel --update
nix-env -iA nixgl.auto.nixGLDefault

# home-manager

nix-channel --add https://github.com/nix-community/home-manager/archive/master.tar.gz home-manager
nix-channel --update
nix-shell '<home-manager>' -A install
$HOME/.nix-profile/etc/profile.d/hm-session-vars.sh

# chaotic nyx

nixos-install --flake .#solitude --option 'extra-substituters' 'https://chaotic-nyx.cachix.org/' --option extra-trusted-public-keys "chaotic-nyx.cachix.org-1:HfnXSw4pj95iI/n17rIDy40agHj12WfF+Gqk6SonIT8="

# nixos-anywhere

nix run github:nix-community/nixos-anywhere -- --flake .#burk --target-host root@192.168.10.154 --generate-hardware-config nixos-generate-config ./hosts/burk/hardware-configuration.nix
