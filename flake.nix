{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    chaotic.url = "github:chaotic-cx/nyx/nyxpkgs-unstable";

	disko.url = "github:nix-community/disko";
	disko.inputs.nixpkgs.follows = "nixpkgs";

    sops-nix.url = "github:Mic92/sops-nix";
	sops-nix.inputs.nixpkgs.follows = "nixpkgs";

    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = {
    nixpkgs,
    chaotic,
	disko,
    home-manager,
    ...
  } @ inputs: {
    nixosConfigurations = {
      solitude = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        specialArgs = inputs;
        modules = [
          ./hosts/solitude
          chaotic.nixosModules.default
          home-manager.nixosModules.home-manager
          {
            home-manager.extraSpecialArgs = inputs;
            home-manager.useUserPackages = true;
            home-manager.users.christer = import ./home/solitude.nix;
            home-manager.backupFileExtension = "backup";
          }
        ];
      };

      burk = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        specialArgs = inputs;
        modules = [
          ./hosts/burk
		  disko.nixosModules.disko
          home-manager.nixosModules.home-manager
          {
            home-manager.extraSpecialArgs = inputs;
            home-manager.useUserPackages = true;
            home-manager.users.christer = import ./home/burk.nix;
            home-manager.backupFileExtension = "backup";
          }
        ];
      };
    };
  };
}
