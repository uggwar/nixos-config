{...}: {
  programs.git = {
    enable = true;
    userName = "Christer Stenbrenden";
    aliases = {
      ci = "commit";
      br = "branch";
      co = "checkout";
      st = "status";
      lg = "log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr)%C(bold blue)<%an>%Creset' --abbrev-commit";
      d = "difftool";
      fuckit = "!git clean -d -x -f && git reset --hard";
    };
    extraConfig = {
      init = {
        defaultBranch = "main";
      };
      pull = {
        rebase = true;
      };
      core = {
        autocrlf = false;
      };
    };
  };
}
