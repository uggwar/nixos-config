{
  config,
  pkgs,
  ...
}: {
  programs.btop = {
    enable = true;
    settings = {
      theme_color = "gruvbox_dark_v2";
      theme_background = false;
    };
  };
}
