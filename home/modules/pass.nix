{
  config,
  pkgs,
  ...
}: {
  programs.password-store = {
    enable = true;
    package = pkgs.pass.withExtensions (ext: with ext; [pass-audit pass-otp pass-import pass-genphrase pass-update pass-tomb]);
  };

  home.sessionVariables = {
    ROFI_PASS_BACKEND = "wtype";
    ROFI_PASS_CLIPBOARD_BACKEND = "wl-clipboard";
  };
}
