{pkgs, ...}: {
  programs.tmux = {
    enable = true;
    shortcut = "a";
    clock24 = true;
    baseIndex = 1;
    sensibleOnTop = true;
    keyMode = "vi";
    mouse = true;
    plugins = [pkgs.tmuxPlugins.sensible];
    extraConfig = ''
         unbind-key j
         unbind-key k
         unbind-key h
         unbind-key l
         bind-key j select-pane -D
         bind-key k select-pane -U
         bind-key h select-pane -L
         bind-key l select-pane -R

         set -g default-terminal "xterm-256color"
         set -ga terminal-overrides ",*256col*:Tc"
         set -ga terminal-overrides '*:Ss=\E[%p1%d q:Se=\E[ q'
         set-environment -g COLORTERM "truecolor"
         set-option -g status "on"

      set -g status-style "bg=#272626,fg=#858885"
      set -g window-status-format " #I:#W"
      set -g window-status-current-format " #[fg=yellow bold]#I:#W"
      set -g status-right "#[fg=yellow]#H #[fg=black bold bg=yellow] %H:%M "
      set -g status-left "[#[fg=blue bold]#S#[fg=default]] "
    '';
  };
}
