{
  config,
  pkgs,
  ...
}: {
  programs.fish = {
    enable = true;
    interactiveShellInit = ''
      set fish_greeting
      direnv hook fish | source
    '';
    functions = {
      pythonEnv = {
        description = "start a nix-shell with the given python packages";
        argumentNames = "pythonVersion";
        body = ''
          if set -q argv[2]
          set argv $argv[2..-1]
          end

          for el in $argv
          set ppkgs $ppkgs "python"$pythonVersion"Packages.$el"
          end

          nix-shell -p $ppkgs
        '';
      };
    };
  };
}
