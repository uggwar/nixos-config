{
  config,
  pkgs,
  ...
}: {
  programs.neovim = {
    enable = true;

    viAlias = true;
    vimAlias = true;
    vimdiffAlias = true;

    extraPackages = with pkgs; [
      lua-language-server
      ripgrep
      just
      fzf
      wl-clipboard
      gcc
      go
      nixd
    ];
  };
}
