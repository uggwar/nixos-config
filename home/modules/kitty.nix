{
  config,
  pkgs,
  ...
}: {
  programs.kitty = {
    enable = true;
    themeFile = "gruvbox-dark-hard";
    settings = {
      scrollback_lines = 25000;
      enable_audio_bell = false;
      cursor_shape = "block";
      shell_integration = "no-cursor";
      font_family = "Hack Nerd Font";
      bold_font = "auto";
      italic_font = "auto";
      bold_italic_font = "auto";
      font_size = "12.0";
      disable_ligatures = "always";
    };
    keybindings = {
      "ctrl+shift+equal" = "change_font_size all +1.0";
      "ctrl+shift+minus" = "change_font_size all -1.0";
    };
  };
}
