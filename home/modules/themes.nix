{
  config,
  pkgs,
  lib,
  ...
}: {
  qt.enable = true;
  qt.platformTheme.name = "gtk";
  qt.style.name = "Nordic";
  qt.style.package = pkgs.nordic;

  home.pointerCursor = {
    name = "Adwaita";
    package = pkgs.adwaita-icon-theme;
    size = 24;
    x11 = {
      enable = true;
      defaultCursor = "Adwaita";
    };
  };

  gtk = {
    enable = true;
    font = {
      name = "Ubuntu";
      size = 11;
      package = pkgs.ubuntu_font_family;
    };
    theme = {
      name = "Nordic";
      package = pkgs.nordic;
    };
    iconTheme = {
      name = "Nordzy";
      package = pkgs.nordzy-icon-theme;
    };
    cursorTheme = {
      name = "Adwaita";
      package = pkgs.adwaita-icon-theme;
    };
    gtk3.extraConfig = {
      Settings = ''
        gtk-application-prefer-dark-theme=1
      '';
    };
    gtk4.extraConfig = {
      Settings = ''
        gtk-application-prefer-dark-theme=1
      '';
    };
  };

  xdg = {
    userDirs = {
      enable = true;
      publicShare = null;
      templates = null;
      createDirectories = true;
    };
  };
}
