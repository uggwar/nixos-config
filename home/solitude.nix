{
  config,
  pkgs,
  inputs,
  ...
}: rec {
  home.username = "christer";
  home.homeDirectory = "/home/christer";
  home.stateVersion = "23.11";

  nixpkgs.config.allowUnfree = true;

  imports = [
    ./modules/fish.nix
    ./modules/git.nix
    ./modules/neovim.nix
    ./modules/tmux.nix
  ];

  programs.git.userEmail = "christer@uggwar.net";

  home.sessionVariables = {
  };

  home.sessionPath = [
    "/home/${home.username}/.local/bin"
  ];

  home.file = {
    ".local/share/flatpak/overrides/global".text = ''
      [Context]
      filesystems=/run/current-system/sw/share/X11/fonts:ro;/nix/store:ro
    '';
  };

  programs.home-manager.enable = true;
}
