{
  config,
  pkgs,
  inputs,
  ...
}: rec {
  home.username = "christer";
  home.homeDirectory = "/home/christer";
  home.stateVersion = "24.11";

  nixpkgs.config.allowUnfree = true;

  imports = [
    ./modules/fish.nix
    ./modules/git.nix
    ./modules/neovim.nix
    ./modules/tmux.nix
  ];

  programs.git.userEmail = "christer@uggwar.net";

  home.sessionVariables = {
  };

  home.sessionPath = [
    "/home/${home.username}/.local/bin"
  ];

  programs.home-manager.enable = true;
}
