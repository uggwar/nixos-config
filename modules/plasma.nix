{
  config,
  pkgs,
  ...
}: {
  xdg.portal.extraPortals = with pkgs; [kdePackages.xdg-desktop-portal-kde];

  # Enable the KDE Plasma Desktop Environment.
  services.displayManager.sddm.enable = true;
  services.displayManager.sddm.wayland.enable = true;
  services.desktopManager.plasma6.enable = true;
  services.displayManager.sddm.theme = "breeze";

  # Exclude some packages
  environment.plasma6.excludePackages = with pkgs.kdePackages; [
    discover
    elisa
    gwenview
    khelpcenter
  ];

  environment.systemPackages = with pkgs; [
	kdePackages.filelight
	kdePackages.breeze-gtk
  ];

  # Enable sound with pipewire.
  services.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    jack.enable = true;
  };
}
