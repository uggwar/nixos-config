{
  config,
  pkgs,
  ...
} @ inputs: {
  systemd.extraConfig = ''
    DefaultTimeoutStopSec=10s
  '';

  hardware.bluetooth.enable = true;
  hardware.enableAllFirmware = true;

  # do garbage collection weekly to keep disk usage low
  nix.gc = {
    automatic = true;
    dates = "weekly";
    options = "--delete-older-than 7d";
  };

  nix.nixPath = ["nixpkgs=${inputs.nixpkgs}"];

  environment.pathsToLink = [
    "/share/nix-direnv"
  ];

  environment.systemPackages = with pkgs; [
    man-pages
    man-pages-posix
  ];

  # print a nice little summary
  system.activationScripts.diff = {
    supportsDryActivation = true;
    text = ''
      ${pkgs.nvd}/bin/nvd --nix-bin-dir=${pkgs.nix}/bin diff \
      /run/current-system "$systemConfig"
    '';
  };

  nix.settings = {
    auto-optimise-store = true;
    keep-outputs = true;
    keep-derivations = true;
    experimental-features = ["nix-command" "flakes"];
  };

  # Select internationalisation properties.
  time.timeZone = "Europe/Oslo";

  # All in @users can request real-time priority
  security.pam.loginLimits = [
    {
      domain = "@users";
      item = "rtprio";
      type = "-";
      value = 1;
    }
  ];

  i18n.defaultLocale = "en_US.UTF-8";
  i18n.supportedLocales = [
    "en_US.UTF-8/UTF-8"
    "nb_NO.UTF-8/UTF-8"
  ];
  i18n.extraLocaleSettings = {
    LC_LANG = "en_US.UTF-8";
    LC_TIME = "nb_NO.UTF-8";
    LC_NUMERIC = "nb_NO.UTF-8";
    LC_COLLATE = "nb_NO.UTF-8";
    LC_MONETARY = "nb_NO.UTF-8";
    LC_PAPER = "nb_NO.UTF-8";
    LC_NAME = "nb_NO.UTF-8";
    LC_ADDRESS = "nb_NO.UTF-8";
    LC_TELEPHONE = "nb_NO.UTF-8";
    LC_MEASUREMENT = "nb_NO.UTF-8";
    LC_IDENTIFICATION = "nb_NO.UTF-8";
  };

  console = {
    earlySetup = true;
    font = "${pkgs.terminus_font}/share/consolefonts/ter-124b.psf.gz";
    packages = with pkgs; [terminus_font];
    keyMap = "us";
  };

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  programs = {
    direnv = {
      enable = true;
      nix-direnv.enable = true;
    };
  };

  environment.variables.EDITOR = "vi";
}
