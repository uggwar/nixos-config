# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).
{
  config,
  sops-nix,
  pkgs,
  ...
}: {
  imports = [
    ./hardware-configuration.nix
    ./disko.nix
    ../../modules/system.nix
    ../../modules/amdgpu.nix
  ];

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  # Kernel
  boot.kernelPackages = pkgs.linuxPackages_6_6_hardened;

  # Enable networking
  networking = {
    useDHCP = false;
    hostName = "burk";
    interfaces = {
      enp2s0.ipv4.addresses = [
        {
          address = "192.168.10.201";
          prefixLength = 16;
        }
      ];
    };
    defaultGateway = {
      address = "192.168.10.1";
      interface = "enp2s0";
    };

    nameservers = ["192.168.10.1"];

    extraHosts = ''
      192.168.10.200 solitude
    '';
  };

  virtualisation.libvirtd.enable = true;
  virtualisation.spiceUSBRedirection.enable = true;

  programs.dconf.enable = true;

  virtualisation.podman = {
    enable = true;
    dockerCompat = true;
    defaultNetwork.settings = {
      dns_enabled = true;
    };
  };

  # GnuPG
  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
  };
  services.pcscd.enable = true;

  services.udisks2.enable = true;

  # Enable the fish shell
  programs.fish.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.christer = {
    isNormalUser = true;
	initialHashedPassword = "$y$j9T$1dkOS.0gEot6.6jz/lkYz1$0nlwBmgiNaCVcON/MTRhW1JRaraypG5RsXOMmyrAqrB";
    description = "Christer Stenbrenden";
    extraGroups = ["users" "wheel" "libvirtd" "kvm"];
    shell = pkgs.fish;
    openssh.authorizedKeys.keys = [
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDo4aV3zmZK+ITOzi45b1LAs0zGC/MHLCNvQqAgjFHbfiYyoDxbYYf+dZwG+176Aem8MGOfe6IGqySl9kVg5QHKBmti46Gm403F0jtPrLJjZVWp8W5sXbj+m1JRUm8C2k57kAjpBEeH77qK+4ihMTltNpcxD3nXgxA611Yg4MId5GTXMonR2gmFir8G/W09nlzTwA+UR8vdKNDBbAJRb4eLpfEU4VRbGakAoOsmtzLrc6nvlq37ptZBWCw6Eh8Z9A6SWwMrcEezPbEX6YHGlgy6ivNit+2ycvOzMEdw0roa43yhWmfGwfWt+gl3WFv73aDl8BCXUyWLdJbaQz4p+0Pn christer@nabber"
	  "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDn1JHH07mSfhSxf2yt7x8PX/x3yyLU2Zdm3Pp414L2b8NG8874m8oGnmhx4OEmMWW1Z8SI3IDz+QkHAiDnszGJ3Mf0Qi7E8rqSAl7F023ClgNfjsp1sZU+ALth5y/+dUm4ag66FWm2maYMgno4Tn+JcCIlIS9BTnuVZSywf7PkAo18aO4QY1gDy58m1n1XxKxlwk3ON1t018xS1ImxYolNZ1fbgjXZfHoOhck7A60CeCjYB0K9jJyeQS1PuQSg5nBXVpPJ1XeZC9eKHp/0yslXFzAFEDvEEBtb560m5bsDm1NMnbO3MRTYL0DSAEbYXoGbuMkTafTTxQkgWXTcA9ErwXPtkuIRMKmPofjyaqnIFfDxDo5/rBxblxkF1VODLTnGJRQDlSQ+Fo71iDq+KEjwz8TTwvoDQW7e+Tnd8D7hJiQue2THcKAan2sZxsSXsyquqJLSNMt0ki1WdTRlla3kCBQNmpFi7tCiQAtThuZz6OltznOkkR+ZKzgxY6E3hDk= uggwar@solitude"
    ];
  };

  environment.systemPackages = let
    btop = pkgs.btop.override {rocmSupport = true;};
  in
    with pkgs; [
      age
      bat
      borgbackup
      btop
      buildah
      cmus
      fd
      file
      fzf
      git
      jq
      just
      lftp
      lsof
      ltrace
      neovim
      netcat-openbsd
      nfdump
      nix-du
      nix-index
      nix-prefetch
      nix-prefetch-git
      nix-tree
      p7zip
      pciutils
      playerctl
      podman-compose
      psmisc
      rclone
      ripgrep
      sops
      stow
      strace
      tcpdump
      tmux
      tree
      udisks2
      unzip
      usbutils
      weechat
      wget
      zip
    ];

  # Enable the OpenSSH daemon.
  services.openssh = {
    enable = true;
    settings = {
      X11Forwarding = true;
      PermitRootLogin = "no"; # disable root login
      PasswordAuthentication = false; # disable password login
    };
    openFirewall = true;
  };

  services.fstrim.enable = true;

  # Open ports in the firewall.
  networking.firewall.enable = true;
  networking.firewall.allowedTCPPorts = [22];
  networking.firewall.allowedUDPPorts = [22];

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "24.11"; # Did you read the comment?
}
