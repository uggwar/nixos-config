# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).
{
  config,
  sops-nix,
  pkgs,
  ...
}: {
  imports = [
    ./hardware-configuration.nix
    ../../modules/radeon
    ../../modules/system.nix
    ../../modules/amdgpu.nix
    ../../modules/plasma.nix
    ../../modules/bindfs-systempath.nix
    ../../modules/steam-devices.nix
    ../../modules/steam.nix
    ../../modules/rivalcfg-udev.nix

    sops-nix.nixosModules.sops
  ];

  # Sops secrets management
  sops.defaultSopsFile = ../../secrets/secrets.yaml;
  sops.defaultSopsFormat = "yaml";

  sops.age.keyFile = "/home/christer/.config/sops/age/keys.txt";
  sops.secrets.borg_passphrase = {
    owner = "christer";
  };

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  # Kernel
  boot.kernelPackages = pkgs.linuxPackages_cachyos;
  boot.kernelParams = ["amdgpu.ppfeaturemask=0xffffffff"];

  # CPU
  powerManagement.cpuFreqGovernor = "performance";

  # Enable networking
  networking = {
    useDHCP = false;
    hostName = "solitude";
    interfaces = {
      enp34s0.ipv4.addresses = [
        {
          address = "192.168.10.200";
          prefixLength = 16;
        }
      ];
    };
    defaultGateway = {
      address = "192.168.10.1";
      interface = "enp34s0";
    };

    nameservers = ["192.168.10.1"];

    extraHosts = ''
      192.168.10.201 burk
    '';
  };

  security.polkit.enable = true;

  xdg.portal.enable = true;

  virtualisation.libvirtd.enable = true;
  virtualisation.spiceUSBRedirection.enable = true;

  programs.dconf.enable = true;

  virtualisation.podman = {
    enable = true;
    dockerCompat = true;
    defaultNetwork.settings = {
      dns_enabled = true;
    };
  };

  programs.gamemode.enable = true;

  # GnuPG
  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
  };
  services.pcscd.enable = true;

  # Enable the X11 windowing system.
  services.xserver.enable = true;

  # configure keymap in x11
  services.xserver.xkb = {
    layout = "uggwar";
    variant = "";
  };

  services.xserver.xkb.extraLayouts.uggwar = {
    description = "customized eurkey for faster norwegian";
    languages = ["eng"];
    symbolsFile = ../../resources/uggwar.xkb;
  };

  services.xserver.wacom.enable = true;

  services.udisks2.enable = true;

  fonts = {
    enableDefaultPackages = true;
    packages = with pkgs; [
      noto-fonts
      noto-fonts-cjk-sans
      noto-fonts-emoji
      font-awesome
      nerd-fonts.iosevka
      nerd-fonts.hack
    ];
    fontconfig = {
      enable = true;
      antialias = true;
      defaultFonts = {
        monospace = ["hack nerd font"];
        serif = ["noto serif"];
        sansSerif = ["noto sans"];
        emoji = ["noto color emoji"];
      };
    };
  };

  # Enable flatpaks as a safety net
  services.flatpak.enable = true;

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Enable touchpad support (enabled default in most desktopManager).
  services.libinput.enable = true;
  services.libinput.mouse.accelProfile = "flat";
  services.libinput.mouse.accelSpeed = "-0.5";

  # Enable the fish shell
  programs.fish.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.christer = {
    isNormalUser = true;
    description = "Christer Stenbrenden";
    extraGroups = ["networkmanager" "gamemode" "wheel" "libvirtd" "input" "users" "kvm"];
    shell = pkgs.fish;
    openssh.authorizedKeys.keys = [
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDo4aV3zmZK+ITOzi45b1LAs0zGC/MHLCNvQqAgjFHbfiYyoDxbYYf+dZwG+176Aem8MGOfe6IGqySl9kVg5QHKBmti46Gm403F0jtPrLJjZVWp8W5sXbj+m1JRUm8C2k57kAjpBEeH77qK+4ihMTltNpcxD3nXgxA611Yg4MId5GTXMonR2gmFir8G/W09nlzTwA+UR8vdKNDBbAJRb4eLpfEU4VRbGakAoOsmtzLrc6nvlq37ptZBWCw6Eh8Z9A6SWwMrcEezPbEX6YHGlgy6ivNit+2ycvOzMEdw0roa43yhWmfGwfWt+gl3WFv73aDl8BCXUyWLdJbaQz4p+0Pn christer@nabber"
    ];
  };

  environment.systemPackages = let
    btop = pkgs.btop.override {rocmSupport = true;};
  in
    with pkgs; [
      age
      alacritty
      alejandra
      bat
      borgbackup
      btop
      buildah
      chromium
      cmus
      corefonts
      crawlTiles
      darktable
      discord
      distrobox
      fd
      file
      fzf
      git
      handbrake
      inkscape
      jq
      just
      keepassxc
      krita
      lftp
      libnotify
      libreoffice-fresh
      libresprite
	  (librewolf.override { cfg.enablePlasmaIntegration = true; })
      lsof
      ltrace
      mpv
      neovim
      netcat-openbsd
      nfdump
      nix-du
      nix-index
      nix-prefetch
      nix-prefetch-git
      nix-tree
      p7zip
      pciutils
      pinentry-qt
      playerctl
      podman-compose
      prismlauncher
      psmisc
      qimgv
      rclone
      ripgrep
      rivalcfg
      signal-desktop
      sops
      spice-gtk
      spotify
      stow
      strace
      tcpdump
      tmux
      tree
      udisks2
      unzip
      usbutils
      virt-manager
      vistafonts
      vlc
      weechat
      wget
      wl-clipboard
      zip

      (lutris.override {
        extraPkgs = pkgs: [
          # List package dependencies here
          wineWowPackages.stable
          winetricks
        ];
      })

      # Create an FHS environment using the command `fhs`, enabling the execution of non-NixOS packages in NixOS!
      (
        let
          base = pkgs.appimageTools.defaultFhsEnvArgs;
        in
          pkgs.buildFHSEnv (base
            // {
              name = "fhs";
              targetPkgs = pkgs: (base.targetPkgs pkgs) ++ [pkgs.pkg-config pkgs.libva];
              profile = "export FHS=1";
              runScript = "bash";
              extraOutputsToInstall = ["dev"];
            })
      )
    ];

  # Enable the OpenSSH daemon.
  services.openssh = {
    enable = true;
    settings = {
      X11Forwarding = true;
      PermitRootLogin = "no"; # disable root login
      PasswordAuthentication = false; # disable password login
    };
    openFirewall = true;
  };

  services.fstrim.enable = true;

  # Open ports in the firewall.
  # Port 27036 Steam client
  # Port 24642 Stardew Valley multiplayer
  networking.firewall.enable = true;
  networking.firewall.allowedTCPPorts = [25565 27036 27037];
  networking.firewall.allowedUDPPorts = [27031 27036 24642];

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.11"; # Did you read the comment?
}
